﻿
using System;

namespace Xilium.CefGlue.Demo.Browser
{
    public sealed class TitleChangedEventArgs : EventArgs
    {
        private readonly string _title;

        public TitleChangedEventArgs(string title)
        {
            _title = title;
        }

        public string Title
        {
            get { return _title; }
        }
    }
}
