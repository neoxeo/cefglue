﻿using System;

namespace Xilium.CefGlue.Demo.Browser
{
    public sealed class TargetUrlChangedEventArgs : EventArgs
    {
        private readonly string _targetUrl;

        public TargetUrlChangedEventArgs(string targetUrl)
        {
            _targetUrl = targetUrl;
        }

        public string TargetUrl
        {
            get { return _targetUrl; }
        }
    }
}
