﻿using System;

namespace Xilium.CefGlue.Demo.Browser
{
    public sealed class AddressChangedEventArgs : EventArgs
    {
        private readonly string _address;

        public AddressChangedEventArgs(string address)
        {
            _address = address;
        }

        public string Address
        {
            get { return _address; }
        }
    }
}
